package db.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;

import db.DatabaseAccess;
import db.dao.UserDAO;
import db.dao.UserMessageDAO;
import db.vo.User;
import db.vo.UserContact;
import db.vo.UserMessage;

public class Populator {
	private UserDAO uDAO;
	private UserMessageDAO mDAO;

	public Populator() throws Exception {
		DatabaseAccess.connect();
		uDAO = new UserDAO();
		mDAO = new UserMessageDAO();

		// populateUsers();
		// populateContacts();
		// populateMessages();
		// getMessages();
		// auth();
	}

	public static void main(String[] args) throws Exception {
		new Populator();
	}

	public void populateUsers() {
		String username = "nick";
		String password = "aaa";
		String email = "@cos.com";
		String description = "jestem nowym użytkownikiem";
		User user;

		// polecam zmieniac wartosc i w zaleznosci od ostatniego id w bazie
		// TODO: mozna by tu wymyslec lepszy sposob generowania unikalnych nazw
		for (int i = 0; i < 20; i++) {
			user = new User();
			user.setUsername(username + " #" + i);
			user.setPassword(password);
			user.setEmail(username + "" + i + "" + email);
			user.setDescription(description + " #" + i);
			uDAO.insertUser(user);
		}
	}

	public void populateContacts() {
		int id = 31;
		int[] cid = { 26, 34, 40 };
		boolean fav = true;
		UserContact uc;

		for (int idc : cid) {
			uc = new UserContact();
			uc.setContact(uDAO.getUser(idc));
			uc.setFavorite(fav);
			uDAO.insertContact(id, uc);
			// System.out.println(idc);
		}

		List<UserContact> list = uDAO.getContactList(id);
		for (UserContact contact : list) {
			System.out.println(contact.getContact().getId());
		}

	}

	public void populateMessages() throws Exception {
		int[] id = { 37, 25 };
		User u1 = uDAO.getUser(id[0]);
		User u2 = uDAO.getUser(id[1]);

		UserMessage message = new UserMessage();
		message.setSender(u1);
		message.setReceiver(u2);
		message.setTimestamp((int) System.currentTimeMillis());
		message.setText("nic? :)");

		File f = new File("src\\db\\test\\Penguins.jpg");
		BufferedImage image = ImageIO.read(f);
		new ImageShowingTool(image);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		message.setImage(new SerialBlob(imageInByte));

		// new ImageShowingTool(mDAO.getImage(message.getImage()));
		mDAO.insertMessage(message);

	}

	public void getMessages() throws Exception {
		int[] id = { 37, 25 };
		User u1 = uDAO.getUser(id[0]);
		User u2 = uDAO.getUser(id[1]);

		List<UserMessage> ms = mDAO.getLastMessages(u1, u2, 1);
		for (UserMessage um : ms) {
			new ImageShowingTool(mDAO.getImage(um.getImage()));
		}
	}

	public void auth() {
		String username = "nick #0";
		String password = "aaa";

		System.out.println(uDAO.authentication(username, password).getUsername());
	}
}
