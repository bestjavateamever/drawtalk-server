package db.test;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImageShowingTool extends JFrame {
	ImageIcon imagei;

	public ImageShowingTool(Image image) {
		super("Image preview");
		this.imagei = new ImageIcon();
		System.err.println(image == null ? "image is null :( " : "");
		this.imagei.setImage(image);

		JLabel label = new JLabel();
		label.setIcon(this.imagei);
		label.setVisible(true);
		add(label);
		super.setBounds(new Rectangle(this.imagei.getIconWidth(), this.imagei.getIconHeight() + 40));

		setVisible(true);
		validate();
		repaint();

	}

}
