package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseAccess {
	static Connection conn = null;

	public static void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "root");
			Statement statement = conn.createStatement();
			statement.executeQuery("use drawtalkdb");
			System.out.println("Polączono z bazą danych.");
		} catch (Exception e) {
			System.err.println("Nie udało się połączyć z bazą danych.");
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		try {
			if (conn.isClosed()) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			System.err.println("Połączenie z bazą nie istnieje. Nastąpi próba połączenia.");
			e.printStackTrace();
			connect();
		}
		return conn;
	}

	public static void disconnect() {
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println("Nie udało się rozłączyć.");
			e.printStackTrace();
		}
	}

}
