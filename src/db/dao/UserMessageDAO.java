package db.dao;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import db.DatabaseAccess;
import db.vo.User;
import db.vo.UserMessage;

public class UserMessageDAO {
	private Connection conn;
	private PreparedStatement st;
	private UserDAO uDAO;

	private static String SELECT_MESSAGE = "select * from user_messages";
	private static String INSERT_MESSAGE = "insert into user_messages (user_s, user_r, ts, image, text) values (?, ?, ?, ?, ?)";

	public UserMessageDAO() {
		this.conn = DatabaseAccess.getConnection();
	}

	public List<UserMessage> getLastMessages(User user, User user2, int count) {
		List<UserMessage> messages = new ArrayList<UserMessage>();
		uDAO = new UserDAO();
		try {
			st = conn
					.prepareStatement("select * from ("
							+ SELECT_MESSAGE
							+ " where (user_s = ? and user_r = ?) or (user_s = ? and user_r = ?)  order by ts desc limit ?)"
							+ " as tb order by tb.ts;");

			st.setInt(1, user.getId());
			st.setInt(2, user2.getId());
			st.setInt(3, user2.getId());
			st.setInt(4, user.getId());
			st.setInt(5, count);
			ResultSet rs = st.executeQuery();
			UserMessage message;

			while (rs.next()) {
				message = new UserMessage();
				message.setId(rs.getInt(1));
				message.setSender(uDAO.getUser(rs.getInt(2)));
				message.setReceiver(uDAO.getUser(rs.getInt(3)));
				message.setTimestamp(rs.getInt(4));
				message.setImage(rs.getBlob(5));
				message.setText(rs.getString(6));
				messages.add(message);
			}

			return messages;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<UserMessage> getMessagesBetween(User user, User user2, int from, int to) {
		List<UserMessage> messages = new ArrayList<UserMessage>();
		uDAO = new UserDAO();
		try {
			st = conn.prepareStatement(SELECT_MESSAGE + " where "
					+ "((user_s = ? and user_r = ?) or (user_s = ? and user_r = ?)) " + "and "
					+ "((ts >= ?) and (ts <= ?)) " + "order by ts desc)");

			st.setInt(1, user.getId());
			st.setInt(2, user2.getId());
			st.setInt(3, user2.getId());
			st.setInt(4, user.getId());
			st.setInt(5, from);
			st.setInt(6, to);
			ResultSet rs = st.executeQuery();
			UserMessage message;

			while (rs.next()) {
				message = new UserMessage();
				message.setId(rs.getInt(1));
				message.setSender(uDAO.getUser(rs.getInt(2)));
				message.setReceiver(uDAO.getUser(rs.getInt(3)));
				message.setTimestamp(rs.getInt(4));
				message.setImage(rs.getBlob(5));
				message.setText(rs.getString(6));
				messages.add(message);
			}

			return messages;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void insertMessage(UserMessage message) {
		try {
			st = conn.prepareStatement(INSERT_MESSAGE);
			st.setInt(1, message.getSender().getId());
			st.setInt(2, message.getReceiver().getId());
			st.setInt(3, message.getTimestamp());
			if (message.getImage() != null) {
				st.setBlob(4, message.getImage());
			} else {
				st.setBinaryStream(4, null);
			}
			st.setString(5, message.getText());
			st.executeUpdate();

		} catch (Exception e) {
			System.err.println("Dodanie wiadomości nie powiodło się.");
			e.printStackTrace();
		}
	}

	// metoda do testow populacji
	public Image getImage(Blob imageB) {
		BufferedImage image = null;
		try {
			InputStream is = imageB.getBinaryStream();

			// // dzialajacy zapis obrazka do pliku (zmniejsza rozmiar xDDD
			// //jakby ktos potrzebowal cos skompresowac :D)
			// FileOutputStream ios = new
			// FileOutputStream("src\\db\\test\\test.jpg");
			// byte[] buffer = new byte[1];
			// while (is.read(buffer) > 0) {
			// ios.write(buffer);
			// }
			// ios.close();
			//
			//

			//
			// dzialajacy odczyt obrazka do image - potrzeba wiecej testów
			//

			image = ImageIO.read(is);

		} catch (Exception e) {
			System.err.println("Pobranie obrazu nie powiodło się.");
			e.printStackTrace();
		}

		return image;
	}
}
