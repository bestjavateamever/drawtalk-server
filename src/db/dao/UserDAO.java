package db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseAccess;
import db.vo.User;
import db.vo.UserContact;

public class UserDAO {
	private Connection conn;
	private PreparedStatement st;

	private static String SELECT_USER = "select * from users";
	private static String SELECT_CONTACTS = "select * from user_contacts where user_id = ?";
	private static String INSERT_USER = "insert into users(username, password, email, description) values (?, ?, ?, ?)";
	private static String INSERT_CONTACT = "insert into user_contacts values (?, ?, ?)";
	private static String DELETE_CONTACT = "delete from user_contacts where user_id = ? and contact_id = ?";

	public UserDAO() {
		this.conn = DatabaseAccess.getConnection();
	}

	public User authentication(String username, String password) {

		try {
			st = conn.prepareStatement(SELECT_USER + " where username like ? and password like ?");
			st.setString(1, username);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();
			rs.last();

			if (rs.getRow() != 0) {
				return getUser(username);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public User getUser(int id) {
		try {
			st = conn.prepareStatement(SELECT_USER + " where id=?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			rs.first();
			int userId = rs.getInt(1);
			String username = rs.getString(2);
			String password = rs.getString(3);
			String email = rs.getString(4);
			String description = rs.getString(5);

			User user = new User();
			user.setId(userId);
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(email);
			user.setDescription(description);
			return user;
		} catch (SQLException e) {
			System.err.println("Operacja pobrania danych użytkownika o id: " + id
					+ " nie powiodła się.");
			e.printStackTrace();
		}
		return null;
	}

	public User getUser(String un) {
		try {
			st = conn.prepareStatement(SELECT_USER + " where username like ?");
			st.setString(1, un);
			ResultSet rs = st.executeQuery();

			rs.first();
			int userId = rs.getInt(1);
			String username = rs.getString(2);
			String password = rs.getString(3);
			String email = rs.getString(4);
			String description = rs.getString(5);

			User user = new User();
			user.setId(userId);
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(email);
			user.setDescription(description);
			return user;
		} catch (SQLException e) {
			System.err.println("Operacja pobrania danych użytkownika o nazwie użytkownika: " + un
					+ " nie powiodła się.");
			e.printStackTrace();
		}

		return null;
	}

	public List<UserContact> getContactList(int userId) {
		List<UserContact> contactList = new ArrayList<UserContact>();
		try {
			st = conn.prepareStatement(SELECT_CONTACTS);
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();
			UserContact uc;

			while (rs.next()) {
				uc = new UserContact();
				uc.setContact(getUser(rs.getInt(2)));
				uc.setFavorite(Boolean.getBoolean(rs.getString(3)));
				contactList.add(uc);
			}

			return contactList;
		} catch (SQLException e) {
			System.err.println("Operacja pobrania kontaktów użytkownika o id: " + userId
					+ " nie powiodła się.");
			e.printStackTrace();
		}
		return null;
	}

	public void insertUser(User user) {
		try {
			st = conn.prepareStatement(INSERT_USER);
			st.setString(1, user.getUsername());
			st.setString(2, user.getPassword());
			st.setString(3, user.getEmail());
			st.setString(4, user.getDescription());
			st.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Dodanie użytkownika nie powiodło się.");
			e.printStackTrace();
		}
	}

	public void insertContact(int userId, UserContact uc) {
		try {
			st = conn.prepareStatement(INSERT_CONTACT);
			st.setInt(1, userId);
			st.setInt(2, uc.getContact().getId());
			st.setString(3, uc.isFavorite() + "");
			st.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Dodanie kontaktu nie powiodło się.");
			e.printStackTrace();
		}
	}

	public void deleteContact(int userId, UserContact uc) {
		try {
			st = conn.prepareStatement(DELETE_CONTACT);
			st.setInt(1, userId);
			st.setInt(2, uc.getContact().getId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Usunięcie kontaktu nie powiodło się.");
			e.printStackTrace();
		}
	}

}
