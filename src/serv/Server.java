package serv;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import db.DatabaseAccess;
import msg.Message;

public class Server {
	 
	private static ServerSocket serverSocket; // soket serwera
	public static Set<Message> messageBuffer = Collections.synchronizedSet(new HashSet<Message>()); // kolekcja wiadomosci wysyłanych do serwera
	public static Set<Client> clientList = Collections.synchronizedSet(new HashSet<Client>()); // kolekcja aktualnie polaczonych klientow
	
	public static void main(String[] args) {
		
		DatabaseAccess.connect();
		
		try {
			serverSocket = new ServerSocket(4444); // utworzenie soketu
			 
		} catch (IOException e) {
			System.out.println("Could not listen on port: 4444"); // blad uruchomienia w przypadku uslugi dzialajacej na porcie 4444
		}
				
		try{
			try {
				while(true)
				{
					System.out.println("Oczekiwanie na klienta na porcie " + serverSocket.getLocalPort() + "...");
					Socket socket = serverSocket.accept(); // utworzenie polaczenia oraz watku do komunikacji
		
					Runnable r = new ServerThread(socket, messageBuffer, clientList);
					Thread t = new Thread(r);
					t.start();
					
				}
			} finally {
				serverSocket.close(); // zamkniecie gniazda w przypadku wystapienia wyjatku
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}	

} 