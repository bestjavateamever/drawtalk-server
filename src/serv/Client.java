package serv;

import java.net.Socket;

public class Client {
	private Socket clientSocket;
	private String login;
	private Boolean loggend;
	
	
	public Client(Socket clientSocket, String login, Boolean loggend) {
		super();
		this.clientSocket = clientSocket;
		this.login = login;
		this.loggend = loggend;
		System.out.println("Client added : " + login);
	}
	public Socket getClientSocket() {
		return clientSocket;
	}
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Boolean getLoggend() {
		return loggend;
	}
	public void setLoggend(Boolean loggend) {
		this.loggend = loggend;
	}
}
