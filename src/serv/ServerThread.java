package serv;


import java.net.*;
import java.util.Iterator;
import java.util.Set;
import java.io.*;

import db.dao.UserDAO;
import msg.Message;
import msg.Type;
import msg.UserData;

public class ServerThread implements Runnable {

	private Socket clientSocket;
	private InputStream inputStream ;  
	private ObjectInputStream objectInputStream;
	private OutputStream outputStream;
	private ObjectOutputStream objectOutputStream;
	private Set<Message> messageBuffer;
	private Set<Client> clientList;
	private Client client = null;
	private UserDAO userdao = new UserDAO();
	
	public ServerThread(Socket socket, Set<Message> messageBuffer, Set<Client> clientList)
	{
		this.clientSocket=socket ; 
		this.messageBuffer = messageBuffer;
		this.clientList = clientList;
	}
	
	public void run() {	
		try {
			System.out.println("Po��czony z " + clientSocket.getRemoteSocketAddress());
			inputStream = clientSocket.getInputStream();  
			objectInputStream = new ObjectInputStream(inputStream);
			outputStream = clientSocket.getOutputStream();
			objectOutputStream = new ObjectOutputStream(outputStream);
			new Thread(new SendingThread()).start();
			
			while(true) {
				
				try {
					Message data = (Message)objectInputStream.readObject();
					System.out.println("Message receved");
					switch(data.getType()) {
					case GET_CLIENT_LIST:
						break;
					case LOGIN_REQUEST:
						UserData ur = (UserData)data.getData();
						client = new Client(clientSocket, ur.getUsername(), true);
						if(userdao.authentication(ur.getUsername(), ur.getPassword())!=null) {
							Send(new Message("server", (String)data.getSrc(), true, Type.LOGIN_REQUEST));
							clientList.add(client);
						} else {
							Send(new Message("server", (String)data.getSrc(), false, Type.LOGIN_REQUEST));
						}
						break;
					case REGISTER_REQUEST:
						if(userdao.getUser(((UserData)data.getData()).getUsername())== null) {
							userdao.insertUser(((UserData)data.getData()).toUser());
							Send(new Message("server", (String)data.getSrc(), true, Type.REGISTER_REQUEST));
						} else {
							if(!userdao.getUser(((UserData)data.getData()).getUsername()).getUsername().equals(((UserData)data.getData()).getUsername())) {
								userdao.insertUser(((UserData)data.getData()).toUser());
								Send(new Message("server", (String)data.getSrc(), true, Type.REGISTER_REQUEST));
							} else {
								Send(new Message("server", (String)data.getSrc(), false, Type.REGISTER_REQUEST));
							}
						}
						break;
					case MESSAGE:
						synchronized(messageBuffer) {
							messageBuffer.add(data);
							System.out.println("Message added");
							messageBuffer.notifyAll();
						}
						break;
					default:
						break;
					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EOFException e) {
					synchronized(clientList) {
						if(client!=null)
						clientList.remove(client);
					}
					System.err.println("Disconnected");
					break;
				}
			} 
		} catch(SocketTimeoutException s) { 
			System.out.println("Timed out!");
		} catch(IOException e) { 
			e.printStackTrace();
			if(e.getClass().getName()=="java.net.SocketException") System.out.println("Disconnected");
		}
	}
	
	public void Send(Message msg) {
		
		try {
			objectOutputStream.flush();
			objectOutputStream.writeObject(msg);
			objectOutputStream.reset();
			objectOutputStream.flush();
			System.out.println("Message sent "+msg.getData());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	class SendingThread implements Runnable {
	    @Override
		public void run() {
			try {
				while(true) {
					synchronized(messageBuffer) {
						messageBuffer.wait();
						if(!messageBuffer.isEmpty()) {
							Iterator<Message> iterator = messageBuffer.iterator();
							Message tmp = null;
							while(iterator.hasNext()) {
								tmp = iterator.next();
								if(tmp.getDest().equals(client.getLogin())) {
									Send(tmp);
									messageBuffer.remove(tmp);
									break;
								}
							}
						}
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}