package msg;
import java.io.Serializable;


public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4395423332296203479L;
	private String src;
	private String dest;
	private Object data;
	private Type type;
	
	public Message(String src, String dest, Object data, Type type) {
		super();
		this.src = src;
		this.dest = dest;
		this.data = data;
		this.type = type;
		//if(this.type.equals(Type.MESSAGE))
			System.out.println("Message created : "+ this.src+" "+this.dest+" "+this.data.toString());
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
