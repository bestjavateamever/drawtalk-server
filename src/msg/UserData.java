package msg;

import java.io.Serializable;

import db.vo.User;

public class UserData implements Serializable {
	private String username;
	private String password;
	private String email;
	private String description;

	public UserData(String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.email = null;
		this.description = null;
	}

	public UserData(String username, String password, String email,
			String description) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.description = description;
	}
	
	public User toUser() {
		User u = new User();
		u.setId(0);
		u.setUsername(username);
		u.setPassword(password);
		u.setEmail(email);
		u.setDescription(description);
		
		return u;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
